
class TestModule(object):
    def tests(self):
        return {
            'is_blue': self.is_blue,
        }
    # check if gitven hex value is shade of blue  
    def is_blue(self, value):
        blueValues = ['0000FF', '0000CD', '00008B', '000080', '00008B', '0000CD', '0000FF']
        value = value.upper()
        if value[0] == '#':
            value = value[1:]
        return value in blueValues